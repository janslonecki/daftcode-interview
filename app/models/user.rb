class User < ApplicationRecord
  validates_presence_of :name, :email, :date_of_birth
  validates_uniqueness_of :email, case_sensitive: false
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  
  after_create :send_registration_email
  
  def has_birthday?
    date_of_birth == Date.today
  end

  private

  def send_registration_email
    UserMailer.registration(self).deliver_now if self.email
  end
end
