class UserResource < JSONAPI::Resource
  attributes :name, :email, :date_of_birth
end
