FactoryBot.define do
  factory :user do
    name { 'John' }
    email  { 'test@test.com' }
    date_of_birth { '2012-10-10' }

    trait :having_birthday do
      date_of_birth { Date.today }
    end
  end
end
