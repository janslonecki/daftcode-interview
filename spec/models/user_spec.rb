describe User, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:date_of_birth) }
  it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
  it { is_expected.to allow_value('test@test.com').for(:email) }
  it { is_expected.not_to allow_value('testtest.com').for(:email) }

  describe '.has_birthday?' do
    subject { user.has_birthday? }

    context 'when user has birthday' do
      let(:user) { create(:user, :having_birthday) }
  
      it { is_expected.to be true }
    end

    context 'when user does not have birthday' do
      let(:user) { create(:user) }
  
      it { is_expected.to be false }
    end
  end
end
